"""
To be used with ipython --matplotlib

run job_sim_predaprey.py

"""
import matplotlib.pyplot as plt
from fluidsim.solvers.models0d.predaprey.solver import Simul

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.8
params.time_stepping.t_end = 40

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var('X', sim.Xs + 2.)
sim.state.state_phys.set_var('Y', sim.Ys + 1.)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

fig = plt.gcf()
fig.savefig("fig0.png")

sim.output.print_stdout.plot_XY_vs_time()

fig = plt.gcf()
fig.savefig("fig1.png")

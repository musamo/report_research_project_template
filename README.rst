A latex template for reports on research projects
=================================================

This repository contains files to produce a pdf file for the report of a
research project with latex (pdflatex and bibtex), python and a Makefile.

Setup the environment
---------------------

To compile the latex code, you need a good latex setup. Under a Debian based
OS, install::

  sudo apt install dvipng texlive-latex-extra texlive-fonts-recommended texlive-fonts-extra latexmk
  # and you can free some memory like this
  sudo apt-get --purge remove tex.\*-doc$

To produce the python figures, you first need to install the small python
package ``fluiddyn``. With a decent python environment (with numpy and
matplotlib), use for example the command::

  pip install fluiddyn --user  # or without --user

Download the code
-----------------

To download these files, clone this repository with the program ``hg``::

  hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/report_research_project_template.git

Alternatively, it can be **better to fork the repository and then to clone your
own repository** (you will then be able to push your modifications on Gitlab).

Temporary hack (2019-12-02)
...........................

With hg-git 0.8.13 (get the version with ``hg --version -v``), there is a bug
with push via http. This bug can be fixed by running (with ``hg`` installed
with ``conda-app``)::

  conda activate _env_mercurial
  pip install -U hg+https://dev.heptapod.net/mercurial/hg-git@ed4289e424aa#egg=hg-git

  # old...
  # pip install -U hg+http://bitbucket.com/paugier/hg-git@9c297597140cb6d60af32f85811e6b56555f6e0b#egg=hg-git

Compile to produce the document
-------------------------------

Finally, to compile the pdf, use the command::

  make

Have a look at the Makefile to understand what it does...

Description of the main files
-----------------------------

The main files are:

- ``README.rst``: file containing this text.

- ``Makefile``: describes how to build the pdf; is used by the program
  ``make``.

- In the directory ``latex``:

  * ``report.tex``: file containing the latex code.
  * ``biblio.bib``: file containing the bibliography entries.
  * ``jfm.bst``: file containing the style for the bibliography.

- In the directory ``fig`` (all figures that are not produced by scripts):

  * ``logo_UGA.png``: the logo of the university.

- The directory ``python`` contains python scripts to make figures and tables.
